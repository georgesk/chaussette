# programme Python qui implémente un salon de discussion, côté client
# inspiré de https://www.geeksforgeeks.org/simple-chat-room-using-python/
# © Deepak Srivatsav

# adaptation pour Python3, correction de bugs, traduction :
# Georges Khaznadar, <georgesk@debian.org>

import socket
import select
import sys

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
if len(sys.argv) != 3:
    print ("Usage correct : script, address IP, numéro de port")
    exit()
IP_address = str(sys.argv[1])
Port = int(sys.argv[2])
server.connect((IP_address, Port))
connected= True

while connected:
    """ 
    Il y a deux situations d'entrée possibles. 
    Soit l'utilisateur vient de taper un message à envoyer aux autres,
    soit le serveur envoie un message à afficher à l'écran.

    La fonction select.select renvoie celui des flux qui est 
    disponible pour y lire quelque chose : ça peut être l'entrée standard
    ou le serveur.

    Si le serveur veut envoyer un message, la condition du 'if' sera vraie
    ci-dessous et sa clause sera évaluée. Si le message provient de l'entrée
    standard, au contraire, c'est la clause du 'else' qui sera évaluée.
    """

    for read_socks in select.select([sys.stdin, server],[],[])[0]:
        if read_socks == server:
            message = server.recv(2048)
            if len(message) == 0:
                """Le serveur est déconnecté"""
                connected = False
                print("Le serveur ne répond plus. C'est fini.")
            else:
                print (message.decode("utf-8").strip())
        else:
            message = sys.stdin.readline()
            server.send(message.encode("utf-8"))
            sys.stdout.write("<Moi-même> " + message)
            sys.stdout.flush()
server.close()
