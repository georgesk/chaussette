# programme Python qui implémente un salon de discussion, côté serveur
# inspiré de https://www.geeksforgeeks.org/simple-chat-room-using-python/
# © Deepak Srivatsav

# adaptation pour Python3, correction de bugs, traduction :
# Georges Khaznadar, <georgesk@debian.org>

import socket
import select
import sys
from _thread import *

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

# vérification qu'il y a eu assez d'arguments fournis
if len(sys.argv) != 3:
    print ("Usage correct : script, addresse IP, numéro de port")
    exit()
# prend le premier argument de la ligne de commande comme adresse IP
IP_address = str(sys.argv[1])
# prend le deuxième argument de la ligne de commande comme numéro de port
Port = int(sys.argv[2])

"""
attache le serveur à une adresse IP donnée et au numéro
de port spécifié.
Le client devra connaître ces paramètres.
"""

server.bind((IP_address, Port))

"""
scrute 100 connexions actives. Ce nombre peut être augmenté
selon les besoins.
"""

server.listen(100)

list_of_clients = []

def clientthread(conn, addr):
    """
    Fonction de rappel qui gère un client du salon de chat.
    :param: conn: un objet connexion
    :param: addr: l'adresse IP du client
    """

    # envoie un message au client dont l'objet utilisateur est "conn"
    conn.send("Bienvenue dans le salon de chat !".encode("utf-8"))

    while True:
            try:
                message = conn.recv(2048)
                if message:
                    """préfixe le message par l'adresse IP de l'émetteur"""
                    message_enrichi = "<" + addr[0] + "> "
                    message_enrichi += message.decode("utf-8")

                    """affiche ce message sur le terminal du serveur"""
                    print (message_enrichi.strip())

                    """Diffuse ce message à tous"""
                    broadcast(message_enrichi, conn)

                else:
                    """un message peut n'avoir aucun contenu si la connexion
                    est rompue, dans ce cas on annule la connexion"""
                    remove(conn)

            except:
                continue

"""
Quand on utilise la fonction ci-dessous, on diffuse le message à tous
les clients dont l'objet n'est pas le même que celui qui envoie
le message"""

def broadcast(message, connection):
    for client in list_of_clients:
        if client!=connection:
            try:
                client.send(message.encode("utf-8"))
            except:
                # si le lien est rompu, on annule le client
                client.close()
                remove(client)

"""
La fonction suivante retire simplement l'objet de
la liste qui avait été créée au début du programme
"""

def remove(connection):
    if connection in list_of_clients:
        list_of_clients.remove(connection)
        print("Fermeture de la communication avec le client" + repr(client))

while True:
    """ Accepte une demande de connexion et enregistre deux paramètres,
    "conn" qui est un objet socket pour cet utilisateur, et "addr"
    qui contient l'adresse IP du client qui s'est juste
    connecté"""
    conn, addr = server.accept()

    """Maintient une liste de clients pour la facilité de diffusion
    des messages à tous les gens présents dans la salle de discussion"""
    list_of_clients.append(conn)

    """affiche l'adresse de l'utilisateur qui vient de se connecter"""
    print (addr[0] + " vient de se connecter à l'instant")

    """crée un fil (thread) individuel pour chaque utilisateur
    qui se connecte"""
    start_new_thread(clientthread,(conn,addr))

server.close()
