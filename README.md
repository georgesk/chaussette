# chaussette

Un petit projet en Python, pour faire un chat rudimentaire ...
et plus, si affinités

## petit mode d'emploi, pour commencer :

On déploie le programme serveur, par exemple le fichier serveur_demo.py
(l'exemple provient du commit 686b84e4fdcfb40ee49677236a74f0a318e56336)
et on le lance. Dans cette explication, le serveur a été déployé sur
la machine `freeduc.science`, dont l'adresse IP est `178.32.223.223`

Dans le serveur, on lance ce programme :

```
python3 serveur_demo.py 178.32.223.223 6543
```

Les paramètres sont :

- 178.32.223.223 : l'adresse IP du serveur physique
- 6543 : un numéro de port choisi arbitrairement, en évitant bien sûr
  d'utiliser d'autres ports de communication déjà en service, sur la
  même machine.

Ensuite, on ouvre deux terminaux, sur deux machines distantes
(les deux peuvent aussi être une seule machine bien sûr, mais on
ouvre bien deux terminaux distincts). Dans chaque terminal, on lance cette
même commande :

```
python3 client.py 178.32.223.223 6543
```

On doit faire attention à utiliser la bonne adresse IP et le bon port : le
client doit trouver un service en fonction, à cette adresse et à ce port.

------

Voici quelques « copies d'écran » montrant ce qui se voit ...

### dans le terminal du serveur

```
georgesk@ns348606:~/developpement/chaussette$ python3 serveur_demo.py 178.32.223.223 5432
86.250.242.82 vient de se connecter à l'instant
86.250.242.82 vient de se connecter à l'instant
<86.250.242.82> hello
```

### dans le terminal du premier client

```
georgesk@libres:~/developpement/chaussette$ python3 client.py 178.32.223.223 5432
Bienvenue dans le salon de chat !
H   H EEEEE L     L      OOO       W   W  OOO  RRRR  L     DDDD  !!
H   H E     L     L     O   O      W W W O   O R   R L     D   D !! 
HHHHH EEEEE L     L     O   O      W W W O   O RRRR  L     D   D !! 
H   H E     L     L     O   O  ,,   W W  O   O R   R L     D   D    
H   H EEEEE LLLLL LLLLL  OOO  ,,    W W   OOO  R   R LLLLL DDDD  !!

```

### dans le terminal deux deuxième client

```
georgesk@libres:~/developpement/chaussette$ python3 client.py 178.32.223.223 5432
Bienvenue dans le salon de chat !
hello
<Moi-même> hello

```

## commentaires

Que s'est-il passé pour obtenir ces trois suites de messages ?

- le serveur a d'abord été lancé, sur une machine nommée `ns348606`
- un premier client a été lancé, sur une machine nommée `libres`
- un deuxième client a été lancé, sur la même machine, dans un autre
  terminal, et tout de suite après on a tapé `hello` suivi d'une validation.
- on voit bien la séquence des évènements, sur le terminal du serveur : les
  deux connexions, puis l'envoi du message `hello`
- dans le deuxième terminal client, on voit bien le retour :
  `<Moi-même> hello` ; c'est bien ce qu'on avait tapé
- dans le premier terminal client par contre, au lieu de voir juste
  `hello`, on voit apparaître un gros pavé d'ascii-art, qui est dû à
  une instruction particulière donnée au serveur au cas où celui-ci
  détecte `hello` dans un message émis.

# pistes d'évolution ...

- laisser les clients posséder d'autres propriétés, comme un pseudo pour
  la personne qui y a démarré un chat, et afficher le pseudo avec le message
  quand le message est diffusé
- faire plus de traitements divers portant sur les échanges de messages ;)
